# neo4jhackv408

A little hack to save my christmas, play with Golang and create various dbs in neo4j community version 4.0.8 for debian 10.

The objective is to show you a little trick to create new Databases in neo4j community version 4.0.8 for debian 10, let's go to that directly and then if you want you can read why this post happened. Review cerefully the BACKUP part, without backup you could have a lot of serious problems.

PostData: Experiment carefully and if possible use virtual machines in kvm or something like that.

# Abstract:

Neo4j Community 4.0.8 Does not support the command create database in debian 10, I had to do a little trick on Neo4j Community Edition 4.0.8. In my trick the first step was init the neo4j service:

<mark>sudo service neo4j start</mark>

Second : clear neo4j default db in neo4j running the command:

<mark>MATCH (n) DETACH DELETE n  </mark> : (in neo4j This command deleted all nodes and relationship in my default neo4j graph)

After I deleted all my nodes and relationships I started creating some nodes (To review 
persistence) and I started to move the next folders to custom path folders:

<mark>/var/lib/neo4j/data/databases/neo4j/</mark>

<mark>/var/lib/neo4j/data/databases/system/</mark>

<mark>/var/lib/neo4j/data/transactions/neo4j/</mark>

<mark>/var/lib/neo4j/data/transactions/system/</mark>

Third: I used the commands
sudo service neo4j stop : (After create a test node , I stoped the service, If you don't stop
the service, transactions folder wont be created to the move)

When the service was down I moved the described folder to a MyCustomDbPathFolder:

<mark>sudo mv /var/lib/neo4j/data/databases/neo4j/ MyCustomDbPathFolder/databases/</mark>

<mark>sudo mv /var/lib/neo4j/data/databases/system/ MyCustomDbPathFolder/databases/</mark>

<mark>sudo mv /var/lib/neo4j/data/transactions/neo4j/ MyCustomDbPathFolder/transactions/</mark>

<mark>sudo mv /var/lib/neo4j/data/transactions/system/ MyCustomDbPathFolder/transactions/</mark>

MyCustomDbFolder is the folder to save the graph db in neo4j folder.

Fourth, to reincorporate and switch beetween dbs:

<mark>sudo service neo4j start <mark>: (The service was down stoped the service)

As I moved the folder using mv, the folders:

<mark>/var/lib/neo4j/data/databases/   ,  </mark> <mark>/var/lib/neo4j/data/databases/   ,  </mark>
<mark>/var/lib/neo4j/data/transactions/  and </mark> <mark>/var/lib/neo4j/data/transactions/</mark>

Don't have neo4j or system folders but neo4j service created the folders again, and the db
in neo4j was empthy, then (Without stop the service) I executed:

<mark>sudo rm -rf /var/lib/neo4j/data/databases/neo4j/</mark>

<mark>sudo rm -rf /var/lib/neo4j/data/databases/system/</mark>

<mark>sudo rm -rf /var/lib/neo4j/data/transactions/neo4j/</mark>

<mark>sudo rm -rf /var/lib/neo4j/data/transactions/system/</mark>


<mark>sudo mv MyCustomDbFolder/databases/neo4j/ /var/lib/neo4j/data/databases/ </mark>

<mark>sudo mv MyCustomDbFolder/databases/system/ /var/lib/neo4j/data/databases/ </mark>

<mark>sudo mv MyCustomDbFolder/transactions/neo4j/ /var/lib/neo4j/data/transactions/ </mark>

<mark>sudo mv MyCustomDbFolder/transactions/system/ /var/lib/neo4j/data/transactions/ </mark>

<mark>sudo service neo4j stop </mark>

<mark>sudo service neo4j start </mark>

And my test persistent nodes were available again.

# Tutorial step by step:

The objective in this tutorial is create and switch between two databases (DbMusic, DbChess) in neo4j community version 4.0.8 for debian 10.

### 1: Prepare a clean folder to play with neo4j and various dbs. Use:

<mark>mkdir wrkspc</mark>

<mark>cd wrkspc</mark>

<mark>ls -la</mark>

<div align="center"><img src="rsrcs/images/f1.png" alt="image1"/></div>

### 2: Review debian and community edition is important.
(Community version is full open source, hacks are possible, in neo4j forums for community version
you can find some hacks. Community version is for learning and play, if you think to writte a serious app I suggest you use enterprise, aura and bloom)

<mark>cat /etc/issue</mark>  (To review debian)

In this step my service start down and with data, the objective: explain clearly.

<mark>sudo service neo4j status</mark>

<mark>sudo service neo4j start</mark>

<mark>sudo service neo4j status</mark>

<div align="center"><img src="rsrcs/images/f2.png" alt="image2"/></div>

open <mark>http://localhost:7474/browser/</mark> in your browser

To review version you can use:

<mark>call dbms.components() yield name, versions, edition unwind versions as version return name, version, edition;</mark>

Verify community edition version 4.0.8 for debian 10.

<div align="center"><img src="rsrcs/images/f3.png" alt="image3"/></div>

Verify data existence, I have Houses hackers and animals in this moment and graphdb:

<mark>MATCH (n) return (n);</mark>

<div align="center"><img src="rsrcs/images/f4.png" alt="image4"/></div>

### 3: Creating needed folders: In this step we are in the folder wrkspc created in the step 1.

First stop the service using: 

<mark>sudo service neo4j stop</mark> : IT IS very IMPORTANT:

and now, execute the next commands to create the needed folders:

<mark>mkdir BACKUPS && cd BACKUPS && mkdir databases && mkdir transactions && cd ..</mark>

<mark>mkdir DbMusic && cd DbMusic && mkdir databases && mkdir transactions && cd ..</mark>

<mark>mkdir Dbchess && cd Dbchess && mkdir databases && mkdir transactions && cd ..</mark>

if you use:

<mark>tree</mark>

you should see something like:

<div align="center"><img src="rsrcs/images/f5.png" alt="image5"/></div>

### 4: Moving folders and adding nodes in Dbmusic and Dbchess.

The first movement is the backup of the actual folders (With animals, hackers and houses)

(Remember the service is down):

<mark>sudo mv /var/lib/neo4j/data/databases/neo4j/ BACKUPS/databases/</mark>

<mark>sudo mv /var/lib/neo4j/data/databases/system/ BACKUPS/databases/</mark>

<mark>sudo mv /var/lib/neo4j/data/transactions/neo4j/ BACKUPS/transactions/</mark>

<mark>sudo mv /var/lib/neo4j/data/transactions/system/ BACKUPS/transactions/</mark>

<div align="center"><img src="rsrcs/images/f6.png" alt="image6"/></div>

now run the service using:

<mark>sudo service neo4j start</mark>

And review <mark>http://localhost:7474/browser/</mark> , If all is good Houses, hackers and animals of the step 2 are missing(They are in BACKUPS), And you have a clean db graph to 
create some nodes in Dbmusic:

<div align="center"><img src="rsrcs/images/f7.png" alt="image7"/></div>

Create some nodes for Dbmusic using:

<mark>create (n:MusicalPiece{name:"For Elisa", instrument: "guitar"}) RETURN n.name;</mark>

<mark>create (n:MusicalPiece{name:"Tico Tico", instrument: "guitar"}) RETURN n.name;</mark>

<mark>match(n) return (n);</mark>

<div align="center"><img src="rsrcs/images/f8.png" alt="image8"/></div>

Stop the neo4j service and send the graph data to DbMusic folder created in step 3:

<mark>sudo service neo4j stop</mark>

<mark>sudo mv /var/lib/neo4j/data/databases/neo4j/ DbMusic/databases/</mark>

<mark>sudo mv /var/lib/neo4j/data/databases/system/ DbMusic/databases/</mark>

<mark>sudo mv /var/lib/neo4j/data/transactions/neo4j/ DbMusic/transactions/</mark>

<mark>sudo mv /var/lib/neo4j/data/transactions/system/ DbMusic/transactions/</mark>

And again:

<mark>sudo service neo4j start</mark>

<div align="center"><img src="rsrcs/images/f9.png" alt="image9"/></div>

Review <mark>http://localhost:7474/browser/</mark> , If all is good, the musical pieces of the previous movement are missing(They are in DbMusic), And we have a clean db to create some in DbChess:

Create some nodes for Dbchess:

<mark>create (n:Piece{name:"Pawn", value: "Infinite"}) RETURN n.name;</mark>

<mark>create (n:Piece{name:"Knight", value: "Infinite"}) RETURN n.name;</mark>

<mark>match(n) return (n);</mark>

<div align="center"><img src="rsrcs/images/f10.png" alt="image10"/></div>

now STOP the neo4j service and send the graph data to Dbchess folder created in step 3:

<mark>sudo service neo4j stop</mark>

<mark>sudo mv /var/lib/neo4j/data/databases/neo4j/ Dbchess/databases/</mark>

<mark>sudo mv /var/lib/neo4j/data/databases/system/ Dbchess/databases/</mark>

<mark>sudo mv /var/lib/neo4j/data/transactions/neo4j/ Dbchess/transactions/</mark>

<mark>sudo mv /var/lib/neo4j/data/transactions/system/ Dbchess/transactions/</mark>

<div align="center"><img src="rsrcs/images/f11.png" alt="image11"/></div>

Until here we have created some dbgraphs in neo4j and redirect the information for each dbgraph from neo4j special folders to custom folders with our own ubication.

### 5: Switching between dbgraphs:

At this point, if you run the service and you understand the trick, you know in the next service execution you will have a clean db, and : ¿how switch? is the question.

Answer:

<mark>sudo service neo4j start</mark>  (Start the service is the first step)

Review the brower at <mark>http://localhost:7474/browser/</mark>

<div align="center"><img src="rsrcs/images/f12.png" alt="image12"/></div>

You now you have clean folders in the neo4j special folders, in this reality (Clean folders in 
special neo4j system folders), to swith for example to DbMusic you execute the next commands, all at the same time and at the same order:

<mark>sudo rm -rf /var/lib/neo4j/data/databases/neo4j/</mark>

<mark>sudo rm -rf /var/lib/neo4j/data/databases/system/</mark>

<mark>sudo rm -rf /var/lib/neo4j/data/transactions/neo4j/</mark>

<mark>sudo rm -rf /var/lib/neo4j/data/transactions/system/</mark>

<mark>sudo mv DbMusic/databases/neo4j/ /var/lib/neo4j/data/databases/ </mark>

<mark>sudo mv DbMusic/databases/system/ /var/lib/neo4j/data/databases/ </mark>

<mark>sudo mv DbMusic/transactions/neo4j/ /var/lib/neo4j/data/transactions/ </mark>

<mark>sudo mv DbMusic/transactions/system/ /var/lib/neo4j/data/transactions/ </mark>

<mark>sudo service neo4j stop</mark>

wait a moment and 

<mark>sudo service neo4j start</mark>

<div align="center"><img src="rsrcs/images/f13.png" alt="image13"/></div>

Review the browser: If all is good, you should see the For Elise and Tico Tico musical pieces.

<div align="center"><img src="rsrcs/images/f14.png" alt="image14"/></div>

<div align="center">
<H4>
Keep calm here and think, we are using mv, that means we have zero dbgraph data in the folder Dbmusic, in fact if you run:
</H4>
<mark>tree DbMusic</mark>
</div>

You can see than DbMusic is empthy but neo4j tool show the musical pieces, you will have to save
your data in the DbMusic folder before a switch. BE CAREFULL; if you aren't attentive about what are you doing: you will have a lot of problems and you will lost data in the process.

<div align="center"><img src="rsrcs/images/f15.png" alt="image15"/></div>

Now switch to Dbchess from DbMusic, for this:

First:

<mark>sudo service neo4j stop</mark>

and now SAVE(Not Delete) Dbmusic using (At the same time):

<mark>sudo mv /var/lib/neo4j/data/databases/neo4j/ DbMusic/databases/</mark>

<mark>sudo mv /var/lib/neo4j/data/databases/system/ DbMusic/databases/</mark>

<mark>sudo mv /var/lib/neo4j/data/transactions/neo4j/ DbMusic/transactions/</mark>

<mark>sudo mv /var/lib/neo4j/data/transactions/system/ DbMusic/transactions/</mark>

wait a moment:

<mark>sudo service neo4j start</mark>

<div align="center"><img src="rsrcs/images/f16.png" alt="image16"/></div>

Review the browser

<div align="center"><img src="rsrcs/images/f17.png" alt="image17"/></div>

You moved data to DbMusic, you have clean data in neo4j system files, in this case
you can delete:

<mark>sudo rm -rf /var/lib/neo4j/data/databases/neo4j/</mark>

<mark>sudo rm -rf /var/lib/neo4j/data/databases/system/</mark>

<mark>sudo rm -rf /var/lib/neo4j/data/transactions/neo4j/</mark>

<mark>sudo rm -rf /var/lib/neo4j/data/transactions/system/</mark>

<mark>sudo mv Dbchess/databases/neo4j/ /var/lib/neo4j/data/databases/ </mark>

<mark>sudo mv Dbchess/databases/system/ /var/lib/neo4j/data/databases/ </mark>

<mark>sudo mv Dbchess/transactions/neo4j/ /var/lib/neo4j/data/transactions/ </mark>

<mark>sudo mv Dbchess/transactions/system/ /var/lib/neo4j/data/transactions/ </mark>

<mark>sudo service neo4j stop</mark>

<div align="center"><img src="rsrcs/images/f18.png" alt="image18"/></div>

wait a moment:

<mark>sudo service neo4j start</mark>

Review the browser: !!!!!! You should have the pawn and the Horse Again.

<div align="center"><img src="rsrcs/images/f19.png" alt="image19"/></div>

In the future you can write a .Sh Golang Python And Rust scripts to play with neo4j community.
If you wish, read again with calm, and try understand. You could prepare execution blocks, I advise you to identify well the points of up and down of the service, that helped me and maybe 
it can also help you.

# Why the post ?:

I love neo4j(Aura, Bloom and enterprise are a great idea for serious projects) and community version is full open source, you can play and learn a lot of things in neo4j community. 
The origin of this post was I had to execute a test in a debian pc that only had neo4j community, 
for the test I needed two graphs but I did not have aura access, neo4j enterprise or bloom, 
I had to solve with the resources that I had and I never give up. I was forced by circumstances 
to do the hack. Aditionally I am a passionated software development and this challenge was special for me.

<div align="center">
	<H1>thanks for visiting the repo and good luck in your code.	</H1>
</div>









































